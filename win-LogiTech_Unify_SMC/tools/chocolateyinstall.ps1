﻿
$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$fileLocation = Join-Path $toolsDir 'LogitechUnify.bat'
$url        = ''
$url64      = ''

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'EXE'
  url           = $url
  url64bit      = $url64
  file         = $fileLocation

  softwareName  = 'win-LogiTech_Unify_SMC*'

  checksum      = '82294B7C814D916AAB2A370AC46B3A820FBF3E72A158FDA1604ED942B7143559'
  checksumType  = 'sha256'
  checksum64    = '82294B7C814D916AAB2A370AC46B3A820FBF3E72A158FDA1604ED942B7143559'
  checksumType64= 'sha256'

 
  validExitCodes= @(0, 3010, 1641)
  silentArgs   = ''
}

Install-ChocolateyInstallPackage @packageArgs










    








